<?php

namespace Drupal\log_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\log_statistics\LogData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to output log data.
 */
class LogGraphController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'log_statistics';
  }

  /**
   * Constructs a LogData.
   *
   * @param \Drupal\log_statistics\LogData $log_data
   *   The log data.
   */
  public function __construct(LogData $log_data) {
    $this->log_data = $log_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('log_statistics.data')
    );
  }

  /**
   * Constructs a graph based on number of database logs.
   *
   * Returns graph data to line_chart.js via drupalSettings where 
   * google line chart is generated.
   *
   * @return array
   *   Graph array.
   */
  public function buildGraph() {

    $statistics_info = $this->log_data->logStatisticsData();

    $build['my_div'] = [
      '#markup' => $this->t('Number of logs in the form of graph.'),
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attached' => ['library' => ['log_statistics/log_statistics.library']],
      '#attributes' => [
        'id' => ['log_statistics_graph'],
        'style' => ['width: 500px', 'height: 600px'],
      ],
    ];
    $build['#attached']['drupalSettings']['log_data'] = $statistics_info;
    return $build;
  }

}
