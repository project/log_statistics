<?php

namespace Drupal\log_statistics;

use Drupal\Core\Database\Connection;

/**
 * Outputs log data from log_statistics table.
 */
class LogData {

  /**
	 * The database service.
	 *
	 * @var \Drupal\Core\Database\Connection
	 */
  protected $connection;

  /**
	 * Constructs a database connection.
	 *
	 * @param \Drupal\Core\Database\Connection $connection
	 *   The database service.
	 */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
	 * {@inheritdoc}
	 */
  protected function getModuleName() {
    return 'log_statistics';
  }

  /**
	 * The logdata renders data from log_statistics database table.
	 */
  public function logStatisticsData() {
    $query = $this->connection->select('log_statistics', 'u');
    $fields = [
    'date',
    'emergency',
    'alert',
    'critical',
    'error',
    'warning',
    'notice',
    'info',
    'debug',
    ];
    $query->fields('u', $fields);
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->orderBy('lid','DESC')->limit(10);
    $results = $pager->execute()->fetchAll();
    $output = [];
    foreach ($results as $result) {
      $output[] = [
      'date' => $result->date,
      'emergency' => intval($result->emergency),
      'alert' => intval($result->alert),
      'critical' => intval($result->critical),
      'error' => intval($result->error),
      'warning' => intval($result->warning),
      'notice' => intval($result->notice),
      'info' => intval($result->info),
      'debug' => intval($result->debug),
      ];
    }
    return $output;
  }

}
