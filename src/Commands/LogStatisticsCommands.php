<?php

namespace Drupal\log_statistics\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Database\Database;
use Drush\Commands\DrushCommands;

/**
 * Class LogStatisticsCommands Contains drush commands.
 *
 * @package Drupal\log_statistics\Commands
 */
class LogStatisticsCommands extends DrushCommands {

  /**
   * Command description here.
   *
   * @param array $options
   *   Options to be used with this command.
   *
   * @option format
   *   Display the data in table, csv or JSON format.
   *
   * @option count
   *   Number of records to be printed.
   * @option date-from
   *   The start date
   * @option date-to
   *   The end date
   * @usage log_statistics-show lss
   *   Usage Display all statistics.
   *
   * @command log_statistics:show
   * @aliases lss
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Returns a formatted row.
   */
  public function show(array $options = [
    'format' => 'table',
    'count' => 10,
    'date-from' => '',
    'date-to' => '',
  ]) {
    if ($options['date-from'] !== '' && $options['date-to'] !== '') {
      $query = Database::getConnection()->select('log_statistics', 'w')
        ->fields('w')
        ->condition('date', [$options['date-from'], $options['date-to']], 'BETWEEN')
        ->orderBy('lid', 'DESC');
    }
    else {
      $query = Database::getConnection()->select('log_statistics', 'w')
        ->range(0, $options['count'])
        ->fields('w')
        ->orderBy('lid', 'DESC');
    }
    $result = $query->execute()->fetchAll();
    $table = $this->formatResult($result);
    if (empty($table)) {
      $this->logger()->notice(dt('No log messages available.'));
    }
    else {
      return new RowsOfFields($table);
    }
  }

  /**
   * Formats the data to be displayed.
   *
   * @param array $results
   *   List of all logs.
   *
   * @return array
   *   Formatted list of records.
   */
  protected function formatResult(array $results) {
    $rows = [];
    foreach ($results as $key => $row) {
      $rows[] = [
        'date' => $row->date,
        'emergency' => $row->emergency,
        'alert' => $row->alert,
        'critical' => $row->critical,
        'error' => $row->error,
        'warning' => $row->warning,
        'notice' => $row->notice,
        'info' => $row->info,
        'debug' => $row->debug,
      ];
    }
    return $rows;
  }

}
