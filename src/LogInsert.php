<?php

namespace Drupal\log_statistics;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * Log insertion cron service.
 */
class LogInsert {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * LogInsert constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime.time service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger service.
   */
  public function __construct(StateInterface $state, TimeInterface $time, Connection $connection, LoggerChannelFactory $logger) {
    $this->state = $state;
    $this->time = $time;
    $this->connection = $connection;
    $this->logger = $logger;
  }

  /**
   * Execute insertLogs once a day.
   *
   * @return bool
   *   Returns FALSE if insertLogs already executed.
   */
  public function run() {
    if ($this->shouldRun()) {
      $severity_results = $this->getLogs();
      if ($severity_results && $severity_results > 0) {
        $this->insertLogs($severity_results);
      }
      else {
        $this->logger->get('log_statistics')->notice('Yesterday logs not available.');
      }
    }
    return FALSE;
  }

  /**
   * Check previous log insertion.
   *
   * @return bool
   *   Returns TRUE/FALSE based on log insertion.
   */
  public function shouldRun() {
    $last_run = $this->state->get('log_statistics.last_insert');
    $time = $this->getLogTime();
    $cron_run_date = date('Y-m-d', $time);
    $today_date = date('Y-m-d');
    if ($cron_run_date == $today_date) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Retrieve logs from log_statistics database table.
   *
   * @return array
   *   Array of severity logs.
   */
  protected function getLogs() {
    // Retrieve yesterday's logs.
    $yesterday_date = strtotime("-1 days");
    $begin_of_day = strtotime("today", $yesterday_date);
    $end_of_day = strtotime("tomorrow", $begin_of_day) - 1;
    $query = $this->connection->select('watchdog', 'w');
    $query->fields('w', ['wid', 'timestamp', 'severity'])
      ->condition('w.timestamp', $begin_of_day, '>=')
      ->condition('w.timestamp', $end_of_day, '<=');
    $results = $query->execute()->fetchAll();
    return $results;
  }

  /**
   * Insert logs in log_statistics database table.
   *
   * @param array $severity_results
   *   The severity levels results.
   */
  protected function insertLogs(array $severity_results) {

    // Get logs for each severity.
    $emergency_data = $alert_data = $critical_data = $error_data = 0;
    $warning_data = $notice_data = $info_data = $debug_data = 0;
    $date = date('Y-m-d', strtotime("-1 days"));
    $create_time = \Drupal::time()->getCurrentTime();
    // TO DO check whether $severity_results->severity is an array or not.
    foreach ($severity_results as $key => $value) {

      switch ($value->severity) {
        case RfcLogLevel::EMERGENCY:
          $emergency_data += 1;
          break;

        case RfcLogLevel::ALERT:
          $alert_data += 1;
          break;

        case RfcLogLevel::CRITICAL:
          $critical_data += 1;
          break;

        case RfcLogLevel::ERROR:
          $error_data += 1;
          break;

        case RfcLogLevel::WARNING:
          $warning_data += 1;
          break;

        case RfcLogLevel::NOTICE:
          $notice_data += 1;
          break;

        case RfcLogLevel::INFO:
          $info_data += 1;
          break;

        default:
          $debug_data += 1;
          break;
      }
    }

    // Insert number of logs into log_statistics table.
    $query = $this->connection->insert('log_statistics');
    $query->fields(
      [
        'date',
        'emergency',
        'alert',
        'critical',
        'error',
        'warning',
        'notice',
        'info',
        'debug',
        'created',
      ]
    );
    $query->values(
      [
        $date,
        $emergency_data,
        $alert_data,
        $critical_data,
        $error_data,
        $warning_data,
        $notice_data,
        $info_data,
        $debug_data,
        $create_time,
      ]
    );
    $query->execute();
    $this->setLogTime();
  }

  /**
   * Set Log time.
   */
  protected function setLogTime() {
    $this->state->set('log_statistics.last_insert', $this->time->getRequestTime());
  }

  /**
   * Get Log time.
   *
   * @return int
   *   The timestamp.
   */
  protected function getLogTime() {
    return $this->state->get('log_statistics.last_insert');
  }

}
