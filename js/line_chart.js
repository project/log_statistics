/**
 * @file
 * JavaScript for log statistics chart.
 */

(function ($) {

  Drupal.behaviors.addGoogleLineChart = {
    attach: function () {
      
      // Get log data via drupalSettings.
      var statistics_data = drupalSettings.log_data;
      var data_length = statistics_data.length;

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Date');
        data.addColumn('number', 'Emergency');
        data.addColumn('number', 'Alert');
        data.addColumn('number', 'Critical');
        data.addColumn('number', 'Error');
        data.addColumn('number', 'Warning');
        data.addColumn('number', 'Notice');
        data.addColumn('number', 'Info');
        data.addColumn('number', 'Debug');

        for (var i = 0; i < data_length; i++) {
          data.addRow([
            new Date(statistics_data[i].date),
            statistics_data[i].emergency, 
            statistics_data[i].alert,
            statistics_data[i].critical,
            statistics_data[i].error,
            statistics_data[i].warning,
            statistics_data[i].notice,
            statistics_data[i].info,
            statistics_data[i].debug
          ]);
        }

        var options = {
          title: 'Log Statistics',
          curveType: 'function',
          legend: { position: 'bottom' },
          hAxis: { format: 'd MMM' },
          vAxis: {viewWindowMode: "explicit", viewWindow:{ min: 0 }},
        };

        var chart = new google.visualization.LineChart(document.getElementById('log_statistics_graph'));

        chart.draw(data, options);
      }

    }
  };

})(jQuery);
