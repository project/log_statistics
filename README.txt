==============
Log Statistics
==============

------------
INTRODUCTION
------------
This module logs total number of each logging severity levels (defined in RFC 5424) in a custom table on a day-to-day basis and outputs a graph based on that. The graph is created with the help of Google Charts' AJAX API.

------------
REQUIREMENTS
------------
* This module requires Database logging (dblog) module of Drupal core.
* Please make sure you have already set up a cron job that runs at least once a day. Please visit https://www.drupal.org/docs/7/setting-up-cron/configuring-cron-jobs-using-the-cron-command for more information.

------------
INSTALLATION
------------
Install the Log Statistics module as you would normally install a contributed Drupal module. Please visit https://www.drupal.org/node/1897420 for more information.

-------
WORKING
-------
* This module logs total number of each severity levels (emergency, alert, critical, error, warning, notice and info) on a cron run.
* For e.g. on today's cron run, yesterday's total number of each log is stored in the module's table.
* The stored data is used by Google Charts to prepare a graph (as of now, for last 10 days). The graph can be seen on the /log-statistics page.

-------------
CONFIGURATION
-------------
No configuration is required.

-----------
MAINTAINERS
-----------
* Niral Patel (niral098) - https://www.drupal.org/u/niral098
